/*
 * Originally written r. simic
 * (c) 2011 Industrie It Pty Ltd
 * http://www.industrieit.com/
 *
 * IndustrieIT Pty Ltd licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * The software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.industrieit.jsf.stateless.impl;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

public class SJSFURIBuilder {

	public static String getURI()
	{
		HttpServletRequest req=(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String uri=req.getRequestURI();
		//truncate any jsessionid
		int ind=uri.indexOf(";");
		if (ind>=0)
		{
			uri=uri.substring(0,ind);
		}
		return uri;
	}
}
