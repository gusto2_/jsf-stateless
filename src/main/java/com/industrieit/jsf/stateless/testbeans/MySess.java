/*
 * Originally written r. simic
 * Industrie It Pty Ltd
 * http://www.industrieit.com/
 *
 * IndustrieIT Pty Ltd licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * The software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.industrieit.jsf.stateless.testbeans;

import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="ms")
@SessionScoped
//@Name("ms")
//@Scope(ScopeType.SESSION)
//@BypassInterceptors
public class MySess {

	private int xx=new Random().nextInt(100);
	private String y="";
	
	
	public String getY() {
		System.out.println("getting y");
		return y;
	}

	public void setY(String y) {
		System.out.println("setting y "+y);
		this.y = y;
	}

	public int getXx() {
		return xx;
	}

	public void setXx(int xx) {
		this.xx = xx;
	}

}
