/*
 * Originally written r. simic
 * Industrie It Pty Ltd
 * http://www.industrieit.com/
 *
 * IndustrieIT Pty Ltd licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * The software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.industrieit.jsf.stateless.testbeans;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import com.industrieit.jsf.stateless.impl.SJSFStatePool;

@ManagedBean(name="sampleController")
@RequestScoped
public class SampleController {

	private String message="";
	private String vv="";
	private int xx=0;
	
	public int getXx() {
		return xx;
	}

	public void setXx(int xx) {
		this.xx = xx;
	}

	public String getVv() {
		return vv;
	}

	public void setVv(String vv) {
		this.vv = vv;
	}

	public String getMessage() {
		return message;
	}

	public String getTime()
	{
		return ""+new Date();
	}
	
	public void click()
	{
		this.message="IT WORKS!!!";
	}
	
	public String sleep()
	{
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "slp";
	}

	public void click2()
	{
		this.message="IT WORKS 2!!!";
	}
	
	public ArrayList<Integer> loop(int count)
	{
		ArrayList<Integer> al=new ArrayList<Integer>();
		for (int x=0;x<count;x++)
		{
			al.add(x);
		}
		return al;
	}
	
	public void cli()
	{
		System.out.println("ATTRIBUTE IS "+UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("xxx"));
	}
	
	public String getDiscriminator()
	{
		String str=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("discrim");
		return str;
	}
	
	public String getCacheStats()
	{
		String st=""+SJSFStatePool.getCacheStats();
		return st.substring(1,st.length()-1).replace(",", "<br/>");
	}

}
