/*
 * Originally written r. simic
 * Industrie It Pty Ltd
 * http://www.industrieit.com/
 *
 * IndustrieIT Pty Ltd licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * The software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.industrieit.jsf.stateless.testutil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;


public class TimingPhaseListener implements PhaseListener {

	private static final long serialVersionUID = 1L;
	private final ThreadLocal<Long> tlTime=new ThreadLocal<Long>();
	private final ThreadLocal<Long> initTime=new ThreadLocal<Long>();
	private final ThreadLocal<List<String>> tlList=new ThreadLocal<List<String>>();

	private boolean enabled=true;
	
	public TimingPhaseListener()
	{
            String env=System.getProperty("com.macquarie.runtime.environment");
            if (env==null)
            {
                env="";
            }
		if (env.toLowerCase().equals("local_dev"))
		{
			enabled=true;
		}
	}

	public void beforePhase(PhaseEvent event) {
		tlTime.set(System.currentTimeMillis());
		System.out.println("---------]]]]]]] "+event.getPhaseId());
		if (event.getPhaseId()==PhaseId.RESTORE_VIEW)
		{
			//store the init time for the whole request
			initTime.set(System.currentTimeMillis());
		}
	}

	public void afterPhase(PhaseEvent event) {
		
		//get the start time
		Long st=tlTime.get();
		
		//get the list
		List<String> l=tlList.get();
		if (l==null)
		{
			l=new ArrayList<String>();
			tlList.set(l);
		}

		HttpServletRequest req=(HttpServletRequest) event.getFacesContext().getExternalContext().getRequest();
		
		String str=("Phase: " + event.getPhaseId()+". Duration "+(System.currentTimeMillis()-st)+" ms");
		l.add(str);

		if (event.getPhaseId()==PhaseId.RENDER_RESPONSE)
		{
			HttpServletRequest reqq=(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			
			StringBuffer sb = new StringBuffer(200);
			sb.append("=========").append(reqq.getRequestURI()).append(" - ").append(reqq.getMethod()).append(" ==========\n");
			for (String s : l)
			{
				sb.append(s).append("\n");
			}
			
			//add the total for the whole request
			sb.append("TOTAL: "+(System.currentTimeMillis()-this.initTime.get())+" ms");

			System.out.println(sb.toString());
			tlList.remove();
			tlTime.remove();

			//print out whats in session
			
			Map<String, Object> sm=FacesContext.getCurrentInstance().getExternalContext().getSessionMap();

			System.out.println("#####################################################################");
		}
	}

	public PhaseId getPhaseId() {
		if (enabled)
		{
			return PhaseId.ANY_PHASE;
		}
		else
		{
			return null;
		}
	}

}