# README #

Stateless implementation of the JSF engine based on the [IT Industried Stateless JSF library](http://blog.industrieit.com/2011/11/14/stateless-jsf-high-performance-zero-per-request-memory-overhead/)

Note:

* maybe (?) obsolete with JSF 2.2 introducing stateless views